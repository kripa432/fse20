Badges Applying for: Available, Functional and Reusable

# Why Available
During our study, the source code of Scone was not available due to which we have build to our own model. Making our tool Porpoise available will help the community to advance in the field.

# Why Functional
Our tool can port legacy application to run on Intel SGX.

# Why Reusable
Developers and researchers can use our tool to run their application on Intel SGX.
