# Requirements
Intel 6th generation skylake CPU or later which provide support for intel SGX.

Ubuntu 16.04 

Linux 4.4.0-169

16 GB Ram(Recommended)

For installation please refer [INSTALL.md](INSTALL.md)
