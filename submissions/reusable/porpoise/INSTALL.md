# Install x86_64-linux-musl-gcc

# Install Linux SGX Driver
Install linux SGX driver 2.5
[linux-sgx-driver](https://github.com/intel/linux-sgx-driver/tree/sgx_driver_2.5)

# Install Linux SGX SDK
Install Linux 2.7.1 Open Source Gold Release
[linux-sgx](https://github.com/intel/linux-sgx/tree/sgx_2.7.1)

# Install Graphene-SGX
Install Graphene-SGX v1.0.1
[Graphene-SGX](https://github.com/oscarlab/graphene/tree/v1.0.1)

# Install Panoply
Install Panoply
[Panoply](https://github.com/shwetasshinde24/Panoply)
customSDK 1.6
customDriver

```
git clone git@github.com:kripa432/Panoply.git

$ source /opt/intel/panoply/environment
```
Added bzip2
1. Install at custom location
2. fork the repo



[comment]: <> We need to provide installion script because everything get installed at /opt/intel
[comment]: <> enclave in musl header file
[commnet]: <> linux-sgx-2.0 is not compatible with Porpoise
[comment]: <> Should we clone graphene-sgx into our own repository?

# Install Porpoise

$ git clone git@gitlab.com:kripa432/porpoise.git
make test

SGX require application binaries to be staticaly linked and position independed code.

To run new application and libraries with Porpoise which are not ported yet
1. Pass "-fPIC" flag to compiler to generate position independed code
2. Pass -D_FORTIFY_SOURCE=0 to compiler
3. Copy the static libraries to enclave/lib folder or add path to  library in Makefile with -L <path to library>



